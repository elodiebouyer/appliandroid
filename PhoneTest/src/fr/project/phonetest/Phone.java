package fr.project.phonetest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class Phone extends BroadcastReceiver{

	public Phone() {
		super();
	}

	@Override
	public void onReceive(Context context, Intent intent) {

		TelephonyManager telephonyManager =
				(TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String phoneNumber = telephonyManager.getLine1Number();
		String outGoing = " ";
		String incomingNumber = " ";

		if (Intent.ACTION_NEW_OUTGOING_CALL.equals(intent.getAction()))
			outGoing = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

		String stringExtra = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
		if (TelephonyManager.EXTRA_STATE_RINGING.equals(stringExtra))
			incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

		try {
			Log.d("test", "call api");
			(new CallApi(context,"APPEL", phoneNumber, incomingNumber, outGoing)).run();
			Log.d("test", "api called");
		} catch (Exception e) {
		}
	}

}
