package com.example.smstest;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.AsyncTask;

public class CallApi extends Thread {
	private final static String API_URL = "http://URL_HERE/add";
	private String url = "";
	
    public CallApi(Context context, String Type, String... params) throws Exception {
    	url = API_URL + "?0=" + Type;
    	String  email = "Inconnu";
    	Account[] accounts = AccountManager.get(context).getAccounts();

        for (Account account : accounts) {
            if (account.type.equals("com.google")) {
            	email = account.name;
                break;
            }
        }
        
        url = url + "&1=" + email;
    	for(int i=0; i<params.length; i++){
    		url = url + "&"+ String.valueOf(i+1) + "=" + URLEncoder.encode(params[i],"UTF-8");
    	}
	}
    
    @Override
    public void run(){
    	try {
    		RetrieveAPI api = new RetrieveAPI();
    		api.execute(url);
    	} catch (Exception e ) {
    	}
    	
    }
    
    class RetrieveAPI extends AsyncTask<String, Void, Object> {

        protected Object doInBackground(String... urls) {
            try {
            	URL _url = new URL(url);
        		URLConnection urlConnection = _url.openConnection();
        		urlConnection.getContent();
            }catch(Exception e){}
			return null;
        		
        }

        protected void onPostExecute(Object content) {
        }
    }
} 