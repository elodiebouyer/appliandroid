package com.example.smstest;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

public class SmsObserver {
	public SmsObserver(Context context) {
		ContentObserver observer = new SmsSend(new Handler());
		((SmsSend) observer).setContext(context);
		context.getContentResolver().registerContentObserver(
				Uri.parse("content://sms"), true, observer);
	}
}
